import json
import re
import logging
from typing import Union

import requests

_log = logging.getLogger(__name__)

class SpotifyBridge:
    def __init__(self) -> None:
        self.session: requests.Session = None

    def init_session(self, url: str) -> requests.Session:
        """
        Initializes a session for Spotify API requests.
        """
        _log.info("SpotifyBridge: init session")

        session = requests.Session()

        r = session.get(url)
        result = re.search(
            r"<script id=\"session\" data-testid=\"session\" type=\"application\/json\">({[a-zA-Z0-9_\-\", :]+})<\/script>",
            r.text,
        )

        config = {}
        try:
            config = json.loads(result.group(1))
        except:
            pass

        clientId = config.get("clientId")
        accessToken = config.get("accessToken")

        _log.info(f"SpotifyBridge: session config: {config}")

        session.headers.update(
            {
                "Referer": "https://open.spotify.com/",
                "authorization": f"Bearer {accessToken}",
            }
        )
        self.session = session
        return session

    @staticmethod
    def get_playlist_id(url: str) -> str:
        """
        Get the playlist id from a Spotify playlist url.
        """

        if "playlist" not in url:
            return None

        playlist_id = url.split("/")[-1].split("?")[0]
        _log.info(f"SpotifyBridge: playlist_id for {url}: {playlist_id}")

        return playlist_id

    def get_playlist_details(self, playlist_id: str) -> Union[dict, None]:
        """
        Get the details of a playlist.
        """
        url = f"https://api.spotify.com/v1/playlists/{playlist_id}?fields=collaborative,description,followers(total),images,name,owner(display_name,id,images,uri),public,tracks(items(track.type,track.duration_ms),total),uri&additional_types=track,episode&market=FR"
        _log.info(f"SpotifyBridge: get_playlist_details: playlist url: {url}")

        r = self.session.get(url)
        if r.status_code != 200:
            _log.error(f"SpotifyBridge: failed to get playlist details: status {r.status_code}: {r.text}")

            return None

        try:
            data = json.loads(r.text)
            data["id"] = playlist_id
            return data
        except:
            return None

    def _get_playlist_tracks(
        self, playlist_data: list, offset=0, limit=100
    ) -> Union[list, None]:
        playlist_id = playlist_data.get("id")
        url = f"https://api.spotify.com/v1/playlists/{playlist_id}/tracks?offset={offset}&limit={limit}&additional_types=track,episode&market=FR"

        r = self.session.get(url)
        if r.status_code != 200:
            return None

        try:
            data = json.loads(r.text)
        except:
            return None

        return data

    def get_playlist_tracks(
        self, playlist_data: list, offset=0, limit=100
    ) -> Union[list, None]:
        tracks = []
        remaining = limit
        for i in range(offset, limit, 100):
            current = self._get_playlist_tracks(
                playlist_data, offset=i, limit=(remaining if remaining < 100 else 100)
            )
            if current:
                tracks += current["items"]
            remaining = limit - 100

        return tracks
