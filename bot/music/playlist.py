import os

import redis

from ..tools import RedisStore
from .song import Song


class PlaylistManager:
    VERSION = "1.0.0"

    def __init__(self) -> None:
        self.store = RedisStore(redis.Redis(host=os.getenv("REDIS_HOST"), port=6379))
        if self.store.get("Playlist Manager", "") != self.VERSION:
            self.store["Playlist Manager"] = self.VERSION

    def get_user_playlists(self, user_id: int) -> list:
        u_rec = f"Playlist of {user_id}"
        return self.store.get(u_rec, [])

    def get_playlist_info(self, user_id: int, playlist_id: str) -> list:
        u_rec = f"Playlist {user_id}:{playlist_id}"
        return self.store.get(u_rec, [])

    def add_music_to_playlist(
        self, user_id: int, playlist_id: str, music: Song
    ) -> bool:
        p_rec = f"Playlist {user_id}:{playlist_id}"
        if p_rec not in self.store:
            self.store[p_rec] = []

            u_rec = f"Playlist of {user_id}"
            if u_rec not in self.store:
                self.store[u_rec] = []

            userp = self.store[u_rec]
            self.store[u_rec] = userp + [playlist_id]

        playlist = self.store[p_rec]
        self.store[p_rec] = playlist + [
            {
                "name": music.source.title,
                "duration": music.source.duration,
                "url": music.source.url,
            }
        ]

        return True

    def remove_music_from_playlist(
        self, user_id: int, playlist_id: str, music_id: str
    ) -> bool:
        p_rec = f"Playlist {user_id}:{playlist_id}"
        if p_rec not in self.store:
            return False

        playlist = self.store[p_rec]
        playlist.pop(music_id)
        if playlist == []:
            return self.delete_playlist(user_id, playlist_id)

        self.store[p_rec] = playlist
        return True

    def delete_playlist(self, user_id: int, playlist_id: str) -> bool:
        p_rec = f"Playlist {user_id}:{playlist_id}"
        if p_rec not in self.store:
            return False

        del self.store[p_rec]

        u_rec = f"Playlist of {user_id}"
        userp = self.store[u_rec]
        userp.remove(playlist_id)
        self.store[u_rec] = userp

        return True

    def load_playlist(self, user_id: int, playlist_id: str) -> list[Song]:
        p_rec = f"Playlist {user_id}:{playlist_id}"
        return self.store[p_rec]
