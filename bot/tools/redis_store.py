import json
from collections import MutableMapping

from redis import Redis


class RedisStore(MutableMapping):
    def __init__(self, engine: Redis):
        self._store = engine

    def __getitem__(self, key):
        data = self._store[key].decode()
        try:
            return json.loads(data)
        except:
            return data

    def __setitem__(self, key, value):
        if type(value) in [dict, list]:
            value = json.dumps(value)
        self._store[key] = value.encode()

    def __delitem__(self, key):
        del self._store[key]

    def __iter__(self):
        return iter(self.keys())

    def __len__(self):
        return len(self._store.keys())

    def keys(self):
        return self._store.keys()

    def clear(self):
        self._store.flushdb()
