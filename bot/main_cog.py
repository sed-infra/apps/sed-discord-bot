import json
import os
import random

import discord
import requests
from discord.ext import commands, tasks

GIT_HASH = os.getenv("GIT_HASH", "dev")
BOT_VERSION = os.getenv("BOT_VERSION", "dev")


class Main(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.help_message = f"""
```
Sed Bot - {BOT_VERSION} - {GIT_HASH}

General commands:
!help - Displays all the available commands.
!ping - Pong!
!fuck - Fuck you!
!popcorn - Grab your popcorn!
!version - Displays the current version of the bot.
!restart - Restart the bot.

Music commands:
!join - Join a voice channel.
!leave or !disconnect - Flush the queue and disconnect from voice channel.
!now or !current or !playing - Displays the currently playing song.
!pause - Pauses the currently playing song.
!resume - Resumes a currently paused song.
!stop - Stops playing song and clears the queue.
!skip or !s - Skip the current song.
!queue or !q - Shows the player's queue. You can optionally specify the page to show. Each page contains 10 elements.
!shuffle - Shuffles the queue.
!remove <int> - Removes a song from the queue at a given index.
!loop - Loops the currently playing song. Invoke this command again to unloop the .
!play or !p <song/playlist name or url> - Plays a song.

Playlist commands:
!playlists - List all your playlists.
!playlist <playlist name> - Displays the playlist.
!playlist_add <playlist name> <song name or url> - Adds a song to a playlist.\n    (You can use this command multiple times to add multiple songs.)\n    (Don't forget to quote the song name if it contains spaces.)
!playlist_del <playlist name> <song index> - Removes a song from a playlist.\n    (You can use !playlist to see the index of the song you want to remove.)
!playlist_flush <playlist name> - Deletes a playlist.
!playlist_load <playlist name> - Loads a playlist into the queue.
!playlist_save <playlist name> - Saves the current queue as a playlist.

Spotify commands:
!spotify_import <playlist name> <spotify playlist url> <offset> <limit> - Imports a Spotify playlist.\n    (Offset and limit are optional.)\n    (Offset is the number of songs to skip before importing.)\n    (Limit (offset required) is the number of songs to import.)
```
"""
        self.update_quote.start()

    def cog_unload(self):
        self.update_quote.cancel()

    # some debug info so that we know the bot has started
    @commands.Cog.listener()
    async def on_ready(self):
        pass

    @commands.command(name="help", help="Displays all the available commands")
    async def help(self, ctx):
        await ctx.send(self.help_message)

    @commands.command(name="ping", help="Pong!")
    async def ping(self, ctx):
        await ctx.send("Pong!")

    @commands.command(name="version", help="Current version")
    async def version(self, ctx):
        await ctx.send(f"{BOT_VERSION} - {GIT_HASH}")

    @commands.command(name="restart", help="Restart the bot")
    @commands.has_role("SedController")
    async def restart(self, ctx):
        await ctx.send("Ok! I restart myself.")
        quit()

    @commands.command(name="fuck", help="Fuck you!")
    async def fuck(self, ctx):
        urls = [
            "https://media1.giphy.com/media/XHr6LfW6SmFa0/giphy.gif?cid=790b76113c6152c79a1f153ce4a7d50b9f9a271160018b08&rid=giphy.gif&ct=g",
            "https://media1.giphy.com/media/QGzPdYCcBbbZm/giphy.gif?cid=790b761103c6f0969beb78331a606a2fa4adf377fb59591c&rid=giphy.gif&ct=g",
            "https://media1.giphy.com/media/nJIWGSNwaOBO/giphy.gif?cid=790b7611805874a4f80ce8a2139709f3f4352552e69b4fdc&rid=giphy.gif&ct=g",
            "https://media3.giphy.com/media/xndHaRIcvge5y/giphy.gif?cid=790b7611ab9397beee2e83d4cf4241511cbc271642597755&rid=giphy.gif&ct=g",
            "https://c.tenor.com/0kRkOqvKwBgAAAAC/mr-bean-middle-finger.gif",
        ]
        random.shuffle(urls)
        url = random.choice(urls)
        await ctx.send(url)

    @commands.command(name="popcorn", help="Grab you popcorn!")
    async def popcorn(self, ctx):
        urls = [
            "https://giphy.com/gifs/snl-saturday-night-live-bill-hader-2UvAUplPi4ESnKa3W0?utm_source=media-link&utm_medium=landing&utm_campaign=Media%20Links&utm_term=https://giphy.com/",
            "https://i.gifer.com/5gp.gif",
        ]
        random.shuffle(urls)
        url = random.choice(urls)
        await ctx.send(url)

    @tasks.loop(minutes=5.0)
    async def update_quote(self):
        await self.bot.wait_until_ready()

        response = requests.get("https://zenquotes.io/api/random")
        json_data = json.loads(response.text)
        quote = json_data[0]["q"] + " -" + json_data[0]["a"]

        await self.bot.change_presence(
            status=discord.Status.online, activity=discord.Game(name=quote)
        )
