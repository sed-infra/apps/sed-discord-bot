import asyncio
import functools
from typing import Union

import discord
import yt_dlp
from discord.ext import commands

# Silence useless bug reports messages
# youtube_dl.utils.bug_reports_message = lambda: ''


class YTDLError(Exception):
    pass


class FFmpegPCMAudioLazy(discord.AudioSource):
    """
    A wrapper for discord.FFmpegPCMAudio
    """

    def __init__(self, *args, **kwargs):
        self.args: list = args
        self.kwargs: dict = kwargs
        self.original: discord.FFmpegPCMAudio = None

    def __del__(self):
        self.cleanup()

    def read(self):
        if not self.original:
            self.original = discord.FFmpegPCMAudio(*self.args, **self.kwargs)
        return self.original.read()

    def is_opus(self):
        return False

    def cleanup(self):
        if self.original:
            self.original.cleanup()


class YTDLSource(discord.PCMVolumeTransformer):
    YTDL_OPTIONS = {
        "format": "bestaudio/best",
        "extractaudio": True,
        "audioformat": "mp3",
        "outtmpl": "%(extractor)s-%(id)s-%(title)s.%(ext)s",
        "restrictfilenames": True,
        "playlist": True,
        "nocheckcertificate": True,
        "ignoreerrors": False,
        "logtostderr": False,
        "quiet": False,
        "no_warnings": False,
        "default_search": "auto",
        "source_address": "0.0.0.0",
    }

    FFMPEG_OPTIONS = {
        "before_options": "-reconnect 1 -reconnect_streamed 1 -reconnect_delay_max 5",
        "options": "-vn",
    }

    ytdl = yt_dlp.YoutubeDL(YTDL_OPTIONS)

    def __init__(
        self,
        ctx: commands.Context,
        source: discord.FFmpegPCMAudio,
        *,
        data: dict,
        volume: float = 0.5
    ):
        super().__init__(source, volume)

        self.requester = ctx.author
        self.channel = ctx.channel
        self.data = data

        self.uploader = data.get("uploader")
        self.uploader_url = data.get("uploader_url")
        date = data.get("upload_date")
        self.upload_date = date[6:8] + "." + date[4:6] + "." + date[0:4]
        self.title = data.get("title")
        self.thumbnail = data.get("thumbnail")
        self.description = data.get("description")
        self.duration = self.parse_duration(data.get("duration"))
        self.tags = data.get("tags")
        self.url = data.get("webpage_url")
        self.views = data.get("view_count")
        self.likes = data.get("like_count")
        self.dislikes = data.get("dislike_count")
        self.stream_url = data.get("url")

    def __str__(self):
        return "**{0.title}** by **{0.uploader}**".format(self)

    @classmethod
    async def create_sources(
        cls, ctx: commands.Context, search: str, *, loop: asyncio.BaseEventLoop = None
    ):
        if not (
            search.startswith("http")
            or search.startswith("ytsearch")
            or search.startswith("https")
        ):
            search = search.replace(":", " ")

        loop = loop or asyncio.get_event_loop()

        partial = functools.partial(
            cls.ytdl.extract_info, search, download=False, process=False
        )
        data = await loop.run_in_executor(None, partial)

        if data is None:
            raise YTDLError("Couldn't find anything that matches `{}`".format(search))

        if "entries" not in data:
            process_info = data
        else:
            process_info = None
            for entry in data["entries"]:
                if entry:
                    process_info = entry
                    break

            if process_info is None:
                raise YTDLError(
                    "Couldn't find anything that matches `{}`".format(search)
                )

        if "webpage_url" not in process_info:
            raise YTDLError("Couldn't find anything that matches `{}`".format(search))

        webpage_url = process_info["webpage_url"]
        partial = functools.partial(cls.ytdl.extract_info, webpage_url, download=False)

        try:
            processed_info = await loop.run_in_executor(None, partial)
        except youtube_dl.DownloadError as e:
            raise YTDLError(str(e))

        if processed_info is None:
            raise YTDLError("Couldn't fetch `{}`".format(webpage_url))

        sources = []
        if "entries" not in processed_info:
            sources.append(
                cls(
                    ctx,
                    FFmpegPCMAudioLazy(processed_info["url"], **cls.FFMPEG_OPTIONS),
                    data=processed_info,
                )
            )
        else:
            for e in processed_info["entries"]:
                try:
                    sources.append(
                        cls(
                            ctx,
                            FFmpegPCMAudioLazy(e["url"], **cls.FFMPEG_OPTIONS),
                            data=e,
                        )
                    )
                except IndexError:
                    raise YTDLError(
                        "Couldn't retrieve any matches for `{}`".format(webpage_url)
                    )

        return sources

    @staticmethod
    def parse_duration(duration: Union[int, None]):
        if not duration:
            return "unknown or live"

        duration = int(duration)

        minutes, seconds = divmod(duration, 60)
        hours, minutes = divmod(minutes, 60)
        days, hours = divmod(hours, 24)

        duration = []
        if days > 0:
            duration.append("{} days".format(days))
        if hours > 0:
            duration.append("{} hours".format(hours))
        if minutes > 0:
            duration.append("{} minutes".format(minutes))
        if seconds > 0:
            duration.append("{} seconds".format(seconds))

        return ", ".join(duration)
