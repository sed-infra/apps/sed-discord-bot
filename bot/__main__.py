import os
import logging
import asyncio

import discord
from discord.ext import commands
from dotenv import load_dotenv

discord.utils.setup_logging(level=logging.INFO)

from .main_cog import Main
from .music import Music
from .tools import find_files

load_dotenv()


async def main():

    libopus = find_files("libopus.so", "/usr/lib")
    if libopus:
        discord.opus.load_opus(libopus[0])

    intents = discord.Intents.default()
    intents.message_content = True

    bot = commands.Bot(command_prefix="!", intents=intents)

    # remove the default help command so that we can write out own
    bot.remove_command("help")

    # register the class with the bot
    await bot.add_cog(Main(bot))
    await bot.add_cog(Music(bot))

    # start the bot with our token
    await bot.start(os.getenv("DISCORD_TOKEN"))


if __name__ == "__main__":
    asyncio.run(main())