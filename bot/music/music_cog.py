import sys
import math
import traceback
import logging

import discord
from discord.ext import commands

from .playlist import PlaylistManager
from .song import Song
from .spotify_bridge import SpotifyBridge
from .voice_state import VoiceState
from .yt import YTDLError, YTDLSource

_log = logging.getLogger(__name__)

class Music(commands.Cog):
    def __init__(self, bot: commands.Bot):
        self.bot = bot
        self.voice_states = {}
        self.playlist_manager = PlaylistManager()

    def get_voice_state(self, ctx: commands.Context):
        state = self.voice_states.get(ctx.guild.id)
        if not state:
            state = VoiceState(self.bot, ctx)
            self.voice_states[ctx.guild.id] = state

        return state

    def cog_unload(self):
        for state in self.voice_states.values():
            self.bot.loop.create_task(state.stop())

    def cog_check(self, ctx: commands.Context):
        if not ctx.guild:
            raise commands.NoPrivateMessage(
                "This command can't be used in DM channels."
            )

        return True

    async def cog_before_invoke(self, ctx: commands.Context):
        ctx.voice_state = self.get_voice_state(ctx)

    async def cog_command_error(
        self, ctx: commands.Context, error: commands.CommandError
    ):
        error = getattr(error, 'original', error)
        if isinstance(error, commands.CommandNotFound):
            await ctx.send(f'{ctx.command} has been disabled.')
        elif isinstance(error, commands.DisabledCommand):
            await ctx.send(f'{ctx.command} has been disabled.')
        elif isinstance(error, commands.BadArgument):
            await ctx.send(f'{ctx.command}: bad argument. Check !help command')
        else:
            await ctx.send("An error occurred: {}".format(str(error)))
            _log.error('Exception in command {}:'.format(ctx.command))
            traceback.print_exception(type(error), error, error.__traceback__, file=sys.stderr)

    @commands.command(name="join", invoke_without_subcommand=True)
    async def _join(self, ctx: commands.Context):
        """Joins a voice channel."""

        destination = ctx.author.voice.channel
        if ctx.voice_state.voice:
            await ctx.voice_state.voice.move_to(destination)
            return

        ctx.voice_state.voice = await destination.connect()

    @commands.command(name="leave", aliases=["disconnect"])
    async def _leave(self, ctx: commands.Context):
        """Clears the queue and leaves the voice channel."""

        if not ctx.voice_state.voice:
            return await ctx.send("Not connected to any voice channel.")

        await ctx.voice_state.stop()
        del self.voice_states[ctx.guild.id]

    @commands.command(name="now", aliases=["current", "playing"])
    async def _now(self, ctx: commands.Context):
        """Displays the currently playing song."""
        if not ctx.voice_state.current:
            return await ctx.send("Nothing is currently playing.")

        await ctx.send(embed=ctx.voice_state.current.create_embed())

    @commands.command(name="pause")
    async def _pause(self, ctx: commands.Context):
        """Pauses the currently playing song."""

        if ctx.voice_state.voice.is_playing():
            ctx.voice_state.voice.pause()
            await ctx.message.add_reaction("⏯")

    @commands.command(name="resume")
    async def _resume(self, ctx: commands.Context):
        """Resumes a currently paused song."""

        if ctx.voice_state.voice.is_paused():
            ctx.voice_state.voice.resume()
            await ctx.message.add_reaction("⏯")

    @commands.command(name="stop")
    async def _stop(self, ctx: commands.Context):
        """Stops playing song and clears the queue."""

        ctx.voice_state.songs.clear()

        if ctx.voice_state.voice.is_playing():
            ctx.voice_state.voice.stop()
            await ctx.message.add_reaction("⏹")

    @commands.command(name="skip", aliases=["s"])
    async def _skip(self, ctx: commands.Context):
        """Skip the current song."""

        if not ctx.voice_state.is_playing:
            return await ctx.send("Not playing any music right now...")

        await ctx.message.add_reaction("⏭")
        ctx.voice_state.skip()

    @commands.command(name="queue", aliases=["q"])
    async def _queue(self, ctx: commands.Context, *, page: int = 1):
        """Shows the player's queue.
        You can optionally specify the page to show. Each page contains 10 elements.
        """

        if len(ctx.voice_state.songs) == 0:
            return await ctx.send("Empty queue.")

        items_per_page = 10
        pages = math.ceil(len(ctx.voice_state.songs) / items_per_page)

        start = (page - 1) * items_per_page
        end = start + items_per_page

        queue = ""
        for i, song in enumerate(ctx.voice_state.songs[start:end], start=start):
            queue += "`{0}.` [**{1.source.title}**]({1.source.url})\n".format(
                i + 1, song
            )

        embed = discord.Embed(
            description="**{} tracks:**\n\n{}".format(len(ctx.voice_state.songs), queue)
        ).set_footer(text="Viewing page {}/{}".format(page, pages))
        await ctx.send(embed=embed)

    @commands.command(name="shuffle")
    async def _shuffle(self, ctx: commands.Context):
        """Shuffles the queue."""

        if len(ctx.voice_state.songs) == 0:
            return await ctx.send("Empty queue.")

        ctx.voice_state.songs.shuffle()
        await ctx.message.add_reaction("✅")

    @commands.command(name="remove")
    async def _remove(self, ctx: commands.Context, index: int):
        """Removes a song from the queue at a given index."""

        if len(ctx.voice_state.songs) == 0:
            return await ctx.send("Empty queue.")

        ctx.voice_state.songs.remove(index - 1)
        await ctx.message.add_reaction("✅")

    @commands.command(name="loop")
    async def _loop(self, ctx: commands.Context):
        """Loops the currently playing song.
        Invoke this command again to unloop the song.
        """

        if not ctx.voice_state.is_playing:
            return await ctx.send("Nothing being played at the moment.")

        # Inverse boolean value to loop and unloop.
        ctx.voice_state.loop = not ctx.voice_state.loop
        await ctx.message.add_reaction("✅")

    @commands.command(name="play", aliases=["p"])
    async def _play(self, ctx: commands.Context, *, search: str):
        """Plays a song.
        If there are songs in the queue, this will be queued until the
        other songs finished playing.
        This command automatically searches from various sites if no URL is provided.
        A list of these sites can be found here: https://rg3.github.io/youtube-dl/supportedsites.html
        """

        if not ctx.voice_state.voice:
            await ctx.invoke(self._join)

        ctx.voice_state.renew_player_task()

        async with ctx.typing():
            try:
                sources = await YTDLSource.create_sources(
                    ctx, search, loop=self.bot.loop
                )
            except YTDLError as e:
                await ctx.send(
                    f"An error occurred while processing the request for **{search}**: {str(e)}"
                )
            else:
                for e in sources:
                    song = Song(e)

                    await ctx.voice_state.songs.put(song)
                    await ctx.send("Enqueued {}".format(str(e)))

    @commands.command(name="playlists")
    async def _playlists(self, ctx: commands.Context):
        """Shows all your playlists."""
        playlists = self.playlist_manager.get_user_playlists(ctx.author.id)
        if playlists:
            await ctx.send("Your playlists:\n- " + "\n- ".join(playlists))
        else:
            await ctx.send("You don't have any playlists.")

    @commands.command(name="playlist_info")
    async def _playlist_info(self, ctx: commands.Context, playlist: str, page: int = 1):
        """Shows the songs of a playlist."""
        playlist_data = self.playlist_manager.get_playlist_info(ctx.author.id, playlist)

        if len(playlist) == 0:
            return await ctx.send("Empty playlist.")
        items_per_page = 10
        pages = math.ceil(len(playlist_data) / items_per_page)

        start = (page - 1) * items_per_page
        end = start + items_per_page

        queue = ""
        for i, song in enumerate(playlist_data[start:end], start=start):
            queue += "`{}.` [**{}**]({})\n".format(
                i + 1, song.get("name"), song.get("url")
            )
        embed = discord.Embed(
            description="**{} tracks:**\n\n{}".format(len(playlist_data), queue)
        ).set_footer(text="Viewing page {}/{}".format(page, pages))
        await ctx.send(embed=embed)

    @commands.command(name="playlist_add")
    async def _playlist_add(self, ctx: commands.Context, playlist: str, search: str):
        """Adds a song to a playlist."""
        async with ctx.typing():
            try:
                sources = await YTDLSource.create_sources(
                    ctx, search, loop=self.bot.loop
                )
            except YTDLError as e:
                await ctx.send(
                    f"An error occurred while processing the request for **{search}**: {str(e)}"
                )
            else:
                for e in sources:
                    song = Song(e)
                    try:
                        self.playlist_manager.add_music_to_playlist(
                            ctx.author.id, playlist, song
                        )
                        await ctx.send(
                            "Playlist **{}**: add {}".format(playlist, str(e))
                        )
                    except Exception as e:
                        await ctx.send(f"An error occurred when adding {search} to playlist {playlist}")
                        _log.error('Exception in command playlist_add for playlist {playlist}: {search}')
                        traceback.print_exception(type(e), e, e.__traceback__, file=sys.stderr)

    @commands.command(name="playlist_del")
    async def _playlist_del(self, ctx: commands.Context, playlist: str, index: int):
        """Removes a song from a playlist at a given index."""
        if self.playlist_manager.remove_music_from_playlist(
            ctx.author.id, playlist, index - 1
        ):
            await ctx.message.add_reaction("✅")

    @commands.command(name="playlist_flush")
    async def _playlist_flush(self, ctx: commands.Context, playlist: str):
        """Delete a playlist"""
        if self.playlist_manager.delete_playlist(ctx.author.id, playlist):
            await ctx.message.add_reaction("✅")

    @commands.command(name="playlist_load")
    async def _playlist_load(self, ctx: commands.Context, playlist: str):
        """Load a playlist to the queue"""
        playlist_data = self.playlist_manager.get_playlist_info(ctx.author.id, playlist)

        if len(playlist) == 0:
            return await ctx.send("Empty playlist.")

        if not ctx.voice_state.voice:
            await ctx.invoke(self._join)
        ctx.voice_state.renew_player_task()

        async with ctx.typing():
            for e in playlist_data:
                url = e.get("url")
                try:
                    sources = await YTDLSource.create_sources(
                        ctx, url, loop=self.bot.loop
                    )
                except YTDLError as e:
                    await ctx.send(
                        f"An error occurred while processing the request for **{url}**: {str(e)}"
                    )
                else:
                    for e in sources:
                        song = Song(e)

                        await ctx.voice_state.songs.put(song)
                        await ctx.send("Enqueued {}".format(str(e)))

    @commands.command(name="playlist_save")
    async def _playlist_save(self, ctx: commands.Context, playlist: str):
        """Save the current queue to a playlist"""

        if len(ctx.voice_state.songs) == 0 and not ctx.voice_state.current:
            return await ctx.send("Empty queue.")

        self.playlist_manager.add_music_to_playlist(
            ctx.author.id, playlist, ctx.voice_state.current
        )
        for s in ctx.voice_state.songs:
            self.playlist_manager.add_music_to_playlist(ctx.author.id, playlist, s)

        await ctx.message.add_reaction("✅")

    @commands.command(name="spotify_import")
    async def _spotify_import(
        self,
        ctx: commands.Context,
        playlist: str,
        spotify_playlist_url: str,
        offset: int = 0,
        limit: int = 100,
    ):
        """Import a Spotify playlist to a playlist"""

        spotify_bridge = SpotifyBridge()
        if not spotify_bridge.init_session(spotify_playlist_url):
            return await ctx.send(
                "Failed to init session or invalid Spotify playlist URL."
            )

        playlist_id = spotify_bridge.get_playlist_id(spotify_playlist_url)
        if not playlist_id:
            return await ctx.send("Failed to get playlist ID.")

        async with ctx.typing():
            playlist_data = spotify_bridge.get_playlist_details(playlist_id)
            if not playlist_data:
                raise Exception("Failed to get playlist details")

            playlist_name = playlist_data.get("name")
            playlist_total_tracks = playlist_data.get("tracks", {}).get("total")

            await ctx.send(
                f"Spotify playlist {playlist_name} (total tracks: {playlist_total_tracks}) - loading {offset}-{offset + limit} tracks..."
            )
            limit = min(limit, playlist_total_tracks)

            tracks = spotify_bridge.get_playlist_tracks(playlist_data, offset, limit)
            for i, t in enumerate(tracks):
                td = t.get("track", {})
                search_sentence = (
                    td.get("name")
                    + " - "
                    + " ".join([a.get("name") for a in td.get("artists", [])])
                )

                percentage = int((i + offset) / playlist_total_tracks * 100)
                await ctx.send(f"- {i + 1}/{len(tracks)} ({percentage}%)")
                await self._playlist_add(ctx, playlist, search_sentence)

    @_join.before_invoke
    @_play.before_invoke
    @_playlist_load.before_invoke
    async def ensure_voice_state(self, ctx: commands.Context):
        if not ctx.author.voice or not ctx.author.voice.channel:
            raise commands.CommandError("You are not connected to any voice channel.")

        if ctx.voice_client:
            if ctx.voice_client.channel != ctx.author.voice.channel:
                raise commands.CommandError("Bot is already in a voice channel.")
