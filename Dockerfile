FROM python:3.9.7-alpine3.14

RUN apk update && apk add --no-cache \
    gcc make musl-dev libffi-dev \
    libsodium ffmpeg opus

ENV IN_DOCKER=1

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY requirements.txt .

RUN adduser -u 1000 -D bot
RUN chown -R bot: /usr/src/app /home/bot

USER bot

# Install python package
RUN python3 -m pip install --upgrade pip && python3 -m pip install --no-cache-dir -r requirements.txt

# Copy the application
COPY . .

env PYTHONUNBUFFERED=1

ARG GIT_HASH
ENV GIT_HASH=${GIT_HASH:-dev}

ARG BOT_VERSION
ENV BOT_VERSION=${BOT_VERSION:-dev}

CMD [ "python3", "-m", "bot" ]

