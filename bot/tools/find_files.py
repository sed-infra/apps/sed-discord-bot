import os


def find_files(filename, search_path):
    result = []

    for root, dir, files in os.walk(search_path):
        for f in files:
            if filename in f:
                result.append(os.path.join(root, f))
    result.sort()
    return result
